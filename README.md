7Wars
=====

7Wars is a previously implemented text-only adventure game that I am now enhacing with graphics and pygame

Right now it is still in heavy development and I can't decide to make it side-scrolling only or to add in isotropic elements for a more hack-n-slash game feel. I'll probably try to find a way to switch from side-scrolling to isotropic to add a cool element to it.

To run this all you need is python 2.7 and the latest version of pygame

In the furture I would like to optimize this game for lower-end computers specifically the Raspberry Pi
