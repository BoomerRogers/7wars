
#Import All the Required Modules
try:
	import random, math, os, getopt, pygame, sys, pickle, getopt, copy
	from collections import *
	from socket import *
	from pygame.locals import*
	from vector import *
	import pygame.mixer
# 	import yaml

except ImportError, err:
	print "couldn't load moduel: %s" % (err)
	sys.exit(2)

#Level Handling and Load Image Function

def load_png(name):
	"""Load image and return object"""
	fullname = os.path.join('gamedata/images', name)
	try:
		image = pygame.image.load(fullname)
		if image.get_alpha() is None:
			image = image.convert()
		else:
			image = image.convert_alpha()
	except pygame.error, message:
		print 'Couldn\'t Load the Image:', fullname
		raise SystemExit, message
	return image

def load_sound(name):
	class NoneSound:
		def play(self): pass
	if not pygame.mixer or not pygame.mixer.get_init():
		return NoneSound()
	fullname = os.path.join('gamedata/sounds', name)
	try:
		sound = pygame.mixer.Sound(fullname)
	except pygame.error:
		print ('%s Music Not Found!' % fullname)
		raise SystemExit(str(geterror()))
	return sound

def fill_gradient(surface, color, gradient, rect=None, vertical=True, forward=True):
    """fill a surface with a gradient pattern
   Parameters:
   color -> starting color
   gradient -> final color
   rect -> area to fill; default is surface's rect
   vertical -> True=vertical; False=horizontal
   forward -> True=forward; False=reverse
   
   Pygame recipe: http://www.pygame.org/wiki/GradientCode
   """
    if rect is None: rect = surface.get_rect()
    x1,x2 = rect.left, rect.right
    y1,y2 = rect.top, rect.bottom
    if vertical: h = y2-y1
    else:        h = x2-x1
    if forward: a, b = color, gradient
    else:       b, a = color, gradient
    rate = (
        float(b[0]-a[0])/h,
        float(b[1]-a[1])/h,
        float(b[2]-a[2])/h
    )
    fn_line = pygame.draw.line
    if vertical:
        for line in range(y1,y2):
            color = (
                min(max(a[0]+(rate[0]*(line-y1)),0),255),
                min(max(a[1]+(rate[1]*(line-y1)),0),255),
                min(max(a[2]+(rate[2]*(line-y1)),0),255)
            )
            fn_line(surface, color, (x1,line), (x2,line))
    else:
        for col in range(x1,x2):
            color = (
                min(max(a[0]+(rate[0]*(col-x1)),0),255),
                min(max(a[1]+(rate[1]*(col-x1)),0),255),
                min(max(a[2]+(rate[2]*(col-x1)),0),255)
            )
            fn_line(surface, color, (col,y1), (col,y2))

#Game class for engine
class Game(object):

	def __init__(self):
		pygame.mixer.init()
		
		self.clock = pygame.time.Clock()
		self.game_state = "menu_main"
		
		#initialize Keyboard() class
		self.keyboard = Keyboard()
		
		self.list_update_always = [self.keyboard]
		self.list_update = []
		self.list_draw = []
		
		#The number of times per sec our engine should update
		self.preferred_fps = 60
		
		#Used to print the fps of the game onto console window
		self.fpsTimer = 0.0
		
		#Should Ragnarok print the number of frames the game is running at?
		self.print_frames = False
		
		#the number of milliseconds between prinitng out the frame rate
		self.print_fps_frequency = 1000
		
		#self.level_1 = Level("gamedata/maps/level_1.map")
		self.level_menu_main = Level_menu_main()
	
	def get_keyboard(self):
		return self.keyboard
	
	def go(self):
		running = True
		while running:
			#print frame
			#This is the main game loop
			#Update our clock
			self.clock.tick(self.preferred_fps)
			elasped_milliseconds = self.clock.get_time()
			
			#Print the fps that the game is running at.
			if self.print_frames:
				self.fpsTimer += elapsed_milliseconds
				if self.fpsTimer > self.print_fps_frequency:
					print "FPS: ", self.clock.get_fps()
					self.fpsTimer = 0.0
			
			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					running = False
					
			function = getattr(self, self.game_state)
			function()
			
			#drawing and updating
			
			for obj in self.list_update_always:
				obj.update(17) # put elapsed_milliseconds instead of 17 if you want it to be affected by time
			for obj in self.list_update:
				obj.update(17) # put elapsed_milliseconds instead of 17 if you want it to be affected by time

 			#screen.fill((230,255,255))
			
			for obj in self.list_draw:
				obj.draw(17, screen)
			
			pygame.display.flip()
		pygame.quit()
		
	def menu_main(self):
		self.list_update = [self.level_menu_main]
		self.list_draw = [self.level_menu_main]
	
	def menu_info(self):
		self.list_update = []
		self.list_draw = []
		
	def game(self):
		pass
	
	def menu_loadlevel(self):
		self.list_update = [self.menu_level_loader]
		self.list_draw = [self.menu_level_loader]
		

################################################################################
##Out of Main Game Classes

class UpdateableObj(object):

	"""
	The base class of our game engine.
	Represents an object that can be updated.
	"""
	#the number of objects that have existed so far. Used to create a unique object id.
	__total_id = 0

	def __init__(self, level, update_order = 0):
		self.level = level
		#The order in which this object should update relative to the other objects.
		self.update_order = update_order
		
		#represents the unique identifier for this object
		self.obj_id = self.__total_id
		
		#Keeps track of the total number of objects created since the start of game.
		UpdateableObj.__total_id += 1
		
		#Is the object allowed to update during the update loop?
		self.is_enabled = True
		
		#Does the object stay in one place even if the camera is moving?
		self.is_static = True
		
		#Represents the location of the object in world space.
		self.position = Vec2d(0,0)
		
		#Allows the user to define the object as they want.
		self.tag = ""
		
		#allows to add commands
		self.commands = None
		
		#variables to be used by self.commands
		self.variable_1 = 0
		self.variable_2 = 0
		self.variable_3 = 0
		
	def __str__(self):
		return "{ \nUpdateableObj: \t Update Order: " + str(self.update_order) + "\t Object ID: " \
               + str(self.obj_id) + "\t Is Enabled: " + str(self.is_enabled)+ "\n}"
        
	def update(self, milliseconds):
		#Update the object.
		if not self.commands == None:
			exec self.commands

class DrawableObj(UpdateableObj):
    """An object that represents something that can be drawn."""
    def __init__(self, level, update_order = 0, draw_order = 0):
        super(DrawableObj, self).__init__(level, update_order)
        self.draw_order = draw_order
 
        #Should the object draw during the update loop?
        self.is_visible = True
 
        #The surface rendered onto the screen when draw is called.
        self.image = pygame.surface.Surface((0, 0))
 
        self.offset = Vec2d(0,0)
 
        try:
            self.scale_factor
        except AttributeError:
            self.scale_factor = 1
 
    def show(self):
        """Enable and show the object."""
        self.is_enabled = true
        self.is_visible = true
 
    def hide(self):
        """Disable and hide the object."""
        self.is_enabled = false
        self.is_visible = false
 
    def is_visible_to_camera(self, camera):
        """Is the object visible to the camera?"""
        #if self.image.get_rect().colliderect(camera.rect):
        #   return True
        # ^ doesn't work, don't know why
        return True
 
    def draw(self, milliseconds, surface):
        #Draw the object
        if self.is_static:
            surface.blit(self.image, self.position + self.offset)
        else:
            relative_position = (self.position + self.offset) - self.level.camera.position
            # glich fix, the render makes weird jumps (only for some objects) between two pixels if you dont do this
            relative_position = Vec2d(relative_position.x, relative_position.y - 0.2)
            surface.blit(self.image, relative_position)

#keyboard and keystate class from ragnarok engine
class KeyState(object):
    def __init__(self):
        #Contains a reference to all pressed status of all the keys
        self.key_states = []
 
    def copy(self):
        new_keys = []
        for key in self.key_states:
            new_keys.append(key)
        state_cpy = KeyState()
        state_cpy.key_states = new_keys
        return state_cpy
 
    def query_state(self, key):
        """
       Query the state of a key. True if the key is down, false if it is up.
       key is a pygame key.
       """
        return self.key_states[key]

class Keyboard(UpdateableObj):
    def __init__(self):
        super(Keyboard, self).__init__(None)
        self.current_state = KeyState()
        self.previous_state = KeyState()
        self.current_state.key_states = pygame.key.get_pressed()
 
    def is_down(self, key):
        return self.current_state.query_state(key)
 
    def is_up(self, key):
        return not self.current_state.query_state(key)
 
    def is_clicked(self, key):
        return self.current_state.query_state(key) and (not self.previous_state.query_state(key))
 
    def is_released(self, key):
        return self.previous_state.query_state(key) and (not self.current_state.query_state(key))
 
    def is_any_down(self):
        """Is any button depressed?"""
        for key in range(len(self.current_state.key_states)):
            if self.is_down(key):
                return True
        return False
 
    def is_any_clicked(self):
        """Is any button clicked?"""
        for key in range(len(self.current_state.key_states)):
            if self.is_clicked(key):
                return True
        return False
 
    def update(self, milliseconds):
        keys = pygame.key.get_pressed()
        self.previous_state = self.current_state.copy()
        self.current_state.key_states = keys
        super(Keyboard, self).update(milliseconds)

class Camera(UpdateableObj):
    def __init__(self, level, position):
        super(Camera, self).__init__(level)
 
        self.offset = Vec2d(windowWidth/2, windowHeight/2)
        self.scale = Vec2d(windowWidth, windowHeight)
        self.position = position - self.offset + self.level.player.scale/2
        self.rect = pygame.Rect(self.position, self.scale)
        self.velocity = Vec2d(0,0)
 
        self.image = pygame.Surface((10,10)).convert()
        self.image.fill((0,0,255))
       
    def update(self, milliseconds):
        #time = (milliseconds / 1000.0)
        #self.velocity.y = 0
        #self.velocity.x = 0
        #if main_game.keyboard.is_down(K_w):
        #    self.velocity.y = -200
        #if main_game.keyboard.is_down(K_s):
        #    self.velocity.y = 200
        #if main_game.keyboard.is_down(K_d):
        #    self.velocity.x = 200
        #if main_game.keyboard.is_down(K_a):
        #    self.velocity.x = -200
        #self.position += self.velocity * time
        camera_position = self.position + self.offset
        player_position = self.level.player.position + self.level.player.scale/2
        distance = player_position - camera_position
        cam_x = 0 #150 # allowed_distance_x_from_camera
       
        camera_position.y = player_position.y
        if distance.x > cam_x:
            camera_position.x = player_position.x - cam_x
        if distance.x < -cam_x:
            camera_position.x = player_position.x + cam_x
 
        self.position = camera_position - self.offset
       
        self.rect.topleft = self.position #- self.offset
 
#################### Main Blueprint for the Game Engine ########################

class Level(UpdateableObj):
    def __init__(self, level_file = None, player_position = None):
        super(Level, self).__init__(self)
       
        self.list_update = []
        self.list_draw = []
 
        #Do we need to sort our list of updateable objects?
        self.__do_need_sort_up = False
 
        #Do we need to sort our list of drawable objects?
        self.__do_need_sort_draw = False
 
        self.player_position = player_position
 
        if not level_file == None:
            self.load_level(level_file)
    
    def load_level(self, level_file):
    	self.load_level_file(level_file)
    	self.parse_level_file()
    
    def find_obj_in_list(self, search_list, value, tag_or_id):
    	if tag_or_id == "tag":
    		for obj in search_list:
    			if obj.tag == value:
    				return obj
    	elif tag_or_id == "id":
    		for obj in search_list:
    			if obj.obj_id == value:
    				return obj
    	else:
    		print "error tag_or_id"
    
    def load_level_file(self, level_file):
    	
    	filename = "gamedata/maps/" + level_file #filename
    	stream = open(filename, 'r')	#opening stream
    	yaml_file = yaml.load(stream)	#loading stream
    	#print yaml_file
    	
    	##################################################
    	# this is because YAML doesn't load dicts in the order it
    	# was written and python only does it with OrderedDict()
    	
    	begin_dict = OrderedDict(yaml_file[0])
    	for index, dictionary in enumerate(yaml_file):
    		if not index == 0:
    			begin_dict.update(dictionary)
    	
    	##################################################
    	self.level = begin_dict
    
    def parse_level_file(self):
        self.list_collision = []

        camer_position = Vec2d(0,0)

        ####### temporary ########

        self.background = DrawableObj(self)
        surface = copy.copy(screen)
        fill_gradient(surface, (170,255,255), (255,255,255))
        self.background.image = surface
        self.list_draw.append(self.background)
    	
    	##########################
        self.scale_factor = 3.0 # best to use integer or 1/2 because of rounding faults of the program
        scale_factor = self.scale_factor

        for obj in self.level:
        
            obj_postion = Vec2d(0,0)
            
            if "position" in self.level[obj]:
                obj_position = Vec2d(self.level[obj]["position"][0]*scale_factor, self.level[obj]["position"][1]*scale_factor)
            
            obj_offset = Vec2d(0,0)
            
            if "offset" in self.level[obj]:
                obj_offset = Vec2d(self.level[obj]["offset"][0]*scale_factor, self.level[obj]["offset"][1]*scale_factor)
            
            obj_scale = Vec2d(0,0)
            
            if "scale" in self.level[obj]:
                obj_scale = Vec2d(self.level[obj]["scale"][0]*scale_factor, self.level[obj]["scale"][1]*scale_factor)
                obj_scale_int = Vec2d(int(self.level[obj]["scale"][0]*scale_factor), int(self.level[obj]["scale"][1]*scale_factor)) # scale values in integers
            
            if "image" in self.level[obj]:
                
                if isinstance(self.level[obj]["image"], basestring):
                    obj_image = load_png(self.level[obj]["image"])
                
                elif isinstance(self.level[obj]["image"][1], list):
                    tile_scale_x = self.level[obj]["image"][1][0]
                    tile_scale_y = self.level[obj]["image"][1][1]
                    tileset = load_tile_table(self.level[obj]["image"][0], tile_scale_x, tile_scale_y)
                    tile_pos_x = self.level[obj]["image"][2][0]
                    tile_pos_y = self.level[obj]["image"][2][1]
                    obj_image = tileset[tile_pos_x][tile_pos_y]
            
            obj_commands = None
            
            if "commands" in self.level[obj]:
                obj_commands = self.level[obj]["commands"]
 
            if "randomclouds" in self.level[obj]:
                tileset_file = self.level[obj]["randomclouds"]
                self.create_clouds(tileset_file)

            if obj == "player":
                if self.player_position == None:
                    self.player_position = obj_position
                    camera_position = copy.copy(obj_position)
                    self.player = Player(self, obj_position)
                    self.list_update.append(self.player)
                    self.list_draw.append(self.player)
                else:
                    camera_position = copy.copy(self.player_position)
                    self.player = Player(self, copy.copy(self.player_position))
                    self.list_update.append(self.player)
                    self.list_draw.append(self.player)
 
            elif "cloud" in self.level[obj]:
                layer = self.level[obj]["cloud"]
                cloud_image = pygame.transform.scale(obj_image, obj_scale_int)
                cloud = Cloud(self, obj_position, obj_scale, cloud_image, layer)
                self.list_draw.append(cloud)
               
            elif "interactive" in self.level[obj]:
                if self.level[obj]["interactive"] == "npc":
                    npc_image_transformed = pygame.transform.scale(obj_image, obj_scale_int)
                    npc_dialog_file = self.level[obj]["dialog"]
                    npc = NPC(self, obj_position, obj_scale, npc_image_transformed, npc_dialog_file)
 
                    self.list_collision.append(npc)
                    self.list_draw.append(npc)
                   
            elif "animation" in self.level[obj]:
                if self.level[obj]["animation"] == "loop":
                    image_sequence = self.level[obj]["image"]
                    tpf = self.level[obj]["tpf"]
                    ani_obj = AnimationLoopObj(self, obj_position, obj_scale, image_sequence, tpf)
                    ani_obj.commands = obj_commands
                    self.list_update.append(ani_obj)
                    self.list_draw.append(ani_obj)
                   
                elif self.level[obj]["animation"] == "oncollision":
                    image_sequence = self.level[obj]["image"]
                    tpf = self.level[obj]["tpf"]
                    condition = self.level[obj]["condition"]
                    ani_obj = AnimationOnCollisionObj(self, obj_position, obj_scale, image_sequence, tpf, condition)
                    ani_obj.commands = obj_commands
                    self.list_collision.append(ani_obj)
                    self.list_update.append(ani_obj)
                    self.list_draw.append(ani_obj)
           
            elif "tile_solids" in self.level[obj]:
                thing = DrawableObj(self)
                thing.position = obj_position
                thing.image = pygame.transform.scale(obj_image, obj_scale_int)
                thing.is_static = False
                self.list_draw.append(thing)
                if "tile_solids" in self.level[obj]:
                    #tile_scale_without_scale_factor = Vec2d(self.level[obj]["tile_scale"][0], self.level[obj]["tile_scale"][1])
                    tile_scale = Vec2d(self.level[obj]["tile_scale"][0]*scale_factor, self.level[obj]["tile_scale"][1]*scale_factor)
                    solid_tiles = self.level[obj]["tile_solids"].split()
                    #print solid_tiles
                    for map_y, line in enumerate(solid_tiles):
                        for map_x, character in enumerate(line):
                            if character == "#":
                                tile_pos = Vec2d(map_x*tile_scale.x, map_y*tile_scale.y)
                                collision_obj = SolidObj(self, tile_pos, tile_scale)
                                self.list_collision.append(collision_obj)
                                #self.list_draw.append(collision_obj) # for debugging
                                #self.list_update.append(collision_obj) # for debugging
                            if character == "^":
                                tile_pos = Vec2d(map_x*tile_scale.x, map_y*tile_scale.y)
                                collision_obj = PlatformObj(self, tile_pos, tile_scale)
                                self.list_collision.append(collision_obj)
                                #self.list_draw.append(collision_obj) # for debugging
                                #self.list_update.append(collision_obj) # for debugging
                               
            elif "solid" in self.level[obj]:
                try:
                    thing = SolidObj(self, obj_position, Vec2d(int(self.level[obj]["solid"][0]*scale_factor), int(self.level[obj]["solid"][1]*scale_factor)))
                    thing.image = pygame.transform.scale(obj_image, obj_scale_int)
                    thing.commands = obj_commands
                    thing.offset = obj_offset
                    self.list_collision.append(thing)
                    if not obj_commands == None:
                        self.list_update.append(thing)
                    self.list_draw.append(thing)
                except TypeError:
                    thing = PlatformObj(self, obj_position, Vec2d(int(self.level[obj]["solid"]*scale_factor),0))
                    thing.image = pygame.transform.scale(obj_image, obj_scale_int)
                    thing.commands = obj_commands
                    thing.offset = obj_offset
                    self.list_collision.append(thing)
                    if not obj_commands == None:
                        self.list_update.append(thing)
                    self.list_draw.append(thing)
            else:
                thing = DrawableObj(self)
                thing.position = obj_position
                thing.image = pygame.transform.scale(obj_image, obj_scale_int)
                thing.is_static = False
                thing.commands = obj_commands
                if not obj_commands == None:
                    self.list_update.append(thing)
                self.list_draw.append(thing)
 
        self.camera = Camera(self, camera_position)
        self.list_update.append(self.camera)
       
    def get_tile(self, x, y):
        try:
            char = self.map[y][x]
        except IndexError:
            return {}
        try:
            return self.key[char]
        except KeyError:
            return {}
 
    def get_bool(self, x, y, name):
        """Tell if the specified flag is set for position on the map."""
       
        value = self.get_tile(x, y).get(name) # return value
        return value in (True, 1, 'true', 'yes', 'True', 'Yes', '1', 'on', 'On')
 
    def get_attribute(self, x, y, name):
        """Tell if the specified flag is set for position on the map."""
       
        value = self.get_tile(x, y).get(name) # return value of that name
        return value
 
    def create_clouds(self, tileset_image):
        # simple extra function too randomly create clouds on the background of the level
        for cloud in range(7):
            tileset = load_tile_table(tileset_image, 48, 48)
            tile_pos_x = random.randint(0, 2)
            tile_pos_y = 0
            obj_image = tileset[tile_pos_x][tile_pos_y]
            obj_position = Vec2d(random.randint(0, 3200), random.randint(0, 1600))
            scale = random.randint(250, 360)
            obj_scale = Vec2d(scale,scale)
            layer = random.randint(4, 9)/10.0
            cloud_image = pygame.transform.scale(obj_image, obj_scale)
            cloud = Cloud(self, obj_position, obj_scale, cloud_image, layer)
            self.list_draw.append(cloud)
 
    def create_gradient_background(self, color_1, color_2):
        self.background = DrawableObj(self)
        surface = copy.copy(screen)
        fill_gradient(surface, color_1, color_2)
        self.background.image = surface
        self.list_draw.append(self.background)
   
    def __draw_cmp(self, obj1, obj2):
        """Defines how our drawable objects should be sorted"""
        if obj1.draw_order > obj2.draw_order:
            return 1
        elif obj1.draw_order < obj2.draw_order:
            return -1
        else:
            return 0
 
    def __up_cmp(self, obj1, obj2):
        """Defines how our updatable objects should be sorted"""
        if obj1.update_order > obj2.update_order:
            return 1
        elif obj1.update_order < obj2.update_order:
            return -1
        else:
            return 0
 
    def __sort_up(self):
        """Sort the updatable objects according to ascending order"""
        if self.__do_need_sort_up:
            self.list_update.sort(self.__up_cmp)
            self.__do_need_sort_up = False
 
    def __sort_draw(self):
        """Sort the drawable objects according to ascending order"""
        if self.__do_need_sort_draw:
            self.list_draw.sort(self.__draw_cmp)
            self.__do_need_sort_draw = False
 
    def update(self, milliseconds):
        """Updates all of the objects in our world."""
        self.__sort_up()
        for obj in self.list_update:
            obj.update(milliseconds)
 
    def draw(self, milliseconds, surface):
        """Draws all of the objects in our world."""
        self.__sort_draw()
        for obj in self.list_draw:
            #Check to see if the object is visible to the camera before doing anything to it.
            if obj.is_static or obj.is_visible_to_camera(self.camera):
                obj.draw(milliseconds, surface)

class Level_menu_main(Level):
    def __init__(self):
        super(Level_menu_main, self).__init__()
		
        self.create_gradient_background((170,255,255), (255,255,255))

        self.menu = Menu_main(self)
        self.list_draw.append(self.menu)
        self.list_update.append(self.menu)


# menu and button classes
# blueprint
class Button(DrawableObj):
    def __init__(self, level, scale, position, image_deselect, image_select, image_selected = None):
        super(Button, self).__init__(level)
       
        self.scale = scale
        self.position = position
        if image_selected == None:
            image_selected = image_select
        self.image_deselect = pygame.transform.scale(image_deselect, scale)
        self.image_select = pygame.transform.scale(image_select, scale)
        self.image_selected = pygame.transform.scale(image_selected, scale)
        self.image = self.image_deselect
        self.selected = False
       
    def select(self):
        self.selected = True
        self.image = self.image_select
       
    def deselect(self):
        self.selected = False
        self.image = self.image_deselect
       
    def clicked_action(self):
        self.image = self.image_selected
        ''' needs to be overwritten'''
        pass

class Button_main_menu(Button):
    def __init__(self, level, scale, position, image_deselect, image_select):
        super(Button_main_menu, self).__init__(level, scale, position, image_deselect, image_select)
 
        self.sound_select = load_sound("menu_hover.wav")
        self.sound_select.set_volume(0.5)
        self.sound_selected = load_sound("menu_hover.mp3")
        self.sound_selected.set_volume(0.5)
 
    def select(self):
        super(Button_main_menu, self).select()
        self.sound_select.play()
   
    def clicked_action(self):
        super(Button_main_menu, self).clicked_action()
        self.sound_selected.play()

class Button_new_game(Button_main_menu):
    def __init__(self, level):
        scale = Vec2d(38*10, 7*10)
        position = Vec2d(50, 5*10)
        image_deselect = load_png("menu/cat.png")
        image_select = load_png("menu/penguine.png")
        
        super(Button_new_game, self).__init__(level, scale, position, image_deselect, image_select)

class Button_load_level(Button_main_menu):
    def __init__(self, level):
        scale = Vec2d(41*10, 7*10)
        position = Vec2d(50, 13*10)
        image_deselect = load_png("menu/cat.png")
        image_select = load_png("menu/penguine.png")
        
        super(Button_load_level, self).__init__(level, scale, position, image_deselect, image_select)

class Button_info(Button_main_menu):
    def __init__(self, level):
        scale = Vec2d(17*10, 7*10)
        position = Vec2d(50, 21*10)
        image_deselect = load_png("menu/cat.png")
        image_select = load_png("menu/penguine.png")
       
        super(Button_info, self).__init__(level, scale, position, image_deselect, image_select)
 
    def clicked_action(self):
        super(Button_info, self).clicked_action()
        print "main main_game begins"
        main_game.game_state = "menu_info"

# BluePrint

class Menu(DrawableObj):
    def __init__(self, level):
        super(Menu, self).__init__(level)
 
        #The keyboard button that selects the button above the currently selected button.
        self.move_up_button = K_UP
 
        #The keyboard button that selects the button below the currently selected button.
        self.move_down_button = K_DOWN
 
        #The keyboard button we will querty for clicked events.
        self.select_button = K_SPACE
 
        #The index of the currently selected button.
        self.current_index = -1
 
        #List of buttons
        self.buttons = []
       
    def update(self, milliseconds):
        #Set the default selected button. This is necessary to to get correct behavior when the menu first starts running.
        if self.current_index == -1:
            self.current_index = 0
            self.buttons[self.current_index].select()
       
        if main_game.keyboard.is_clicked(self.move_up_button):
            self.move_up()
        if main_game.keyboard.is_clicked(self.move_down_button):
            self.move_down()
        if main_game.keyboard.is_released(self.select_button):
            self.buttons[self.current_index].clicked_action()
 
        super(Menu, self).update(milliseconds)
           
    def move_up(self):
        """
       Try to select the button above the currently selected one.
       If a button is not there, wrap down to the bottom of the menu and select the last button.
       """
        old_index = self.current_index
        self.current_index -= 1
        self.__wrap_index()
        self.__handle_selections(old_index, self.current_index)
 
    def move_down(self):
        """
       Try to select the button under the currently selected one.
       If a button is not there, wrap down to the top of the menu and select the first button.
       """
        old_index = self.current_index
        self.current_index += 1
        self.__wrap_index()
        self.__handle_selections(old_index, self.current_index)
       
    def __wrap_index(self):
        """Wraps the current_index to the other side of the menu."""
        if self.current_index < 0:
            self.current_index = len(self.buttons) - 1
        elif self.current_index >= len(self.buttons):
            self.current_index = 0
 
    def __handle_selections(self, old_index, new_index):
        #Don't perform any deselections or selections if the currently selected button hasn't changed.
        if old_index is not new_index:
            #Deselect the old button
            self.buttons[old_index].deselect()
##            print "Button " + str(old_index) + " deselected."
 
            #Select the new button.
            self.buttons[new_index].select()
##            print "Button " + str(new_index) + " selected."
           
    def draw(self, milliseconds, surface):
        for button in self.buttons:
            button.draw(milliseconds, surface)
        super(Menu, self).draw(milliseconds, surface)

class Menu_main(Menu):
    def __init__(self, level):
        super(Menu_main, self).__init__(level)
       
        self.button_new_game = Button_new_game(level)
        self.button_load_level = Button_load_level(level)
        self.button_info = Button_info(level)
        self.buttons = [self.button_new_game, self.button_load_level, self.button_info]
 
 
###############################################################################

pygame.init()

pygame.display.set_icon(pygame.image.load("gamedata/images/menu/cat.png"))

windowWidth, windowHeight = 1280, 720 #854, 480 #640, 360 #750, 500 #1280, 720
window = pygame.display.set_mode((windowWidth,windowHeight))

pygame.display.set_caption('Test Game')
screen = pygame.display.get_surface()

pygame.mouse.set_visible(True)

#This initializes the game
main_game = Game()

#main function
def main():
	main_game.go()

main()
