import pygame, sys, os
from pygame.locals import*

##Object that fixes the absolute path for png images
def load_png(name):
	"""Load image and return object"""
	fullname = os.path.join('gamedata/images', name)
	try:
		image = pygame.image.load(fullname)
		if image.get_alpha() is None:
			image = image.convert()
		else:
			image = image.convert_alpha()
	except pygame.error, message:
		print 'Couldn\'t Load the Image:', fullname
		raise SystemExit, message
	return image

screen_size = (800,600)

class IsoGame(object):
    def  __init__(self):
        pygame.init()
        flags = DOUBLEBUF
        self.surface = pygame.display.set_mode(screen_size, flags)
        self.gamestate = 1
        images = load_png("spaceship2.png")
        frame_rect = pygame.Rect(1,0,125,47)
        self.speed = 2
        self.player_x = 50
        self.player_y = 30
        self.player_image = [None] * 3 #creates the amount of images to store
        
        for im in range(0,3):
            frame_rect.top = im*48+1
            self.player_image[im] = images.subsurface(frame_rect)
        
        self.player_frame = 0 #sets default image to show

        self.penguine_image = load_png("penguine.png")
        self.loop()
    
    def move(self, Dirx, Diry):
    	self.player_x = self.player_x + (Dirx * self.speed)
    	self.player_y = self.player_y + (Diry * self.speed)

    def game_exit(self):
        exit()
    def loop(self):
        while self.gamestate == 1:
            self.player_frame = 0
            for event in pygame.event.get():
                if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                    self.gamestate = 0
            
            #Player Movements in games [S=down][W = up][A = Left][D=Right]
            keys = pygame.key.get_pressed()
            player_anim = 0
            
            if keys[K_s]:
            	self.move(0,1)
            	self.player_frame = 1
            	
            if keys[K_w]:
            	self.move(0,-1)
            	self.player_frame = 1
            	
            if keys[K_d]:
            	self.move(1,0)
            	self.player_frame = 1
            	
            if keys[K_a]:
            	self.move(-1,0)
            	self.player_frame = 1
            
            self.surface.fill((0,0,0))
            self.surface.blit(self.penguine_image, (10,20))
            
            self.surface.blit(self.player_image[self.player_frame], (self.player_x, self.player_y))
            
            self.surface.blit(self.penguine_image, (220,20))
            pygame.display.flip()
        
        self.game_exit()

if __name__ == '__main__':
    IsoGame()
